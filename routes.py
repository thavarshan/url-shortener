from init import app


@app.route('/')
def index():
    """
    Show index page
    """
    return 'Home'

